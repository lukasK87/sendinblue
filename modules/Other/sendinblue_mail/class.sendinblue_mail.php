<?php


class sendinblue_mail extends TransactionalEmailModule
{

    /**
     *
     * Module version. Make sure to increase version any time you add some
     * new functions in this class!
     * @var string
     */
    protected $version = '1.0';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'Sendinblue';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'Sendinblue integration';

    /**
     * @var $sendinblue
     *
     */
    protected $sendinblue;

    /**
     * @var array
     */
    protected $configuration = array(
        'API Key' => array(
            'value' => '',
            'type' => 'input',
            'description' => 'Place your API Key here'
        )
    );

    /**
     * sendinblue_mail constructor.
     */
    public function __construct()
    {

        parent::__construct();
    }


       public function SendEmail(\Mailer $mailer)
    {

        $resipients = array();
        foreach ($mailer->getRecipients() as $rec) {
            $item = array();
            $item['email'] = $rec[0];
            $item['name'] = !empty($rec[1]) ? $rec[1] : $rec[0];
            $resipients[] = $item;
        }


        $bcc = array();
        foreach ($mailer->getBcc() as $rec) {
            $item = array();
            $item['email'] = $rec[0];
            $item['name'] = $rec[1];
            $bcc[] = $item;
        }

        $cc = array();
        foreach ($mailer->getCc() as $rec) {
            $item = array();
            $item['email'] = $rec[0];
            $item['name'] = $rec[1];
            $cc[] = $item;
        }


        $reply_to = array();
        foreach ($mailer->getReplyTo() as $rep) {
            $reply_to = $rep[0];
        }

        $attachments = array();
        foreach ($mailer->GetAttachments() as $attachment) {
            $file = [];
            $file['name'] = $attachment[2];
            if (!$attachment[5]) {
                $file_str = file_get_contents($attachment[0]);
            } else {
                $file_str = $attachment[0];
            }
            $file_str = base64_encode($file_str);
            $file['content'] = $file_str;
            $attachments[] = $file;
        }


        $json = [
            'sender'        =>  ['name'=>$mailer->FromName, 'email'=>$mailer->From],
            'to'            =>  $resipients,
            'htmlContent'   =>  $mailer->Body,
            'textContent'   =>  !empty($mailer->AltBody)? $mailer->AltBody : $mailer->Body,
            'subject'       =>  $mailer->Subject,
            'replyTo'       =>  !empty($reply_to) ? ['email'    =>  $reply_to] : $resipients[0]];
        if(!empty($attachments))
           $json['attachment'] =  $attachments;
        if(!empty($bcc))
            $json['bcc'] =  $bcc;
        if(!empty($cc))
            $json['cc'] =  $cc;

        $json = json_encode($json);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.sendinblue.com/v3/smtp/email",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>$json,
            CURLOPT_HTTPHEADER  => ['api-key:'.$this->configuration['API Key']['value'], 'Content-Type: application/json']
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response = json_decode($response, true);


        if ($err) {
            $mailer->ErrorInfo = 'A Sendinblue error occurred: ' . $err;
            return false;
        } elseif(isset($response['message'])) {
            $mailer->ErrorInfo = 'A Sendinblue error occurred: ' . $response['message'];
            return false;
        }

        return true;

    }


}
